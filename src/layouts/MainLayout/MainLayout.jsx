import React, { Component, PropTypes } from 'react';
import { Router, Route, IndexRoute, Link } from 'react-router';
import styles from './MainLayout.less';
import { Menu, Icon } from 'antd';
const  SubMenu = Menu.SubMenu;

const MainLayout = ({ children }) => {
  return (
    <div className={styles.normal}>
      <div className={styles.head}>
        <h1>猫屋</h1>
      </div>
      <div  className={styles.content + ' mwoa-menu-wraper'}>
      <Menu className={styles.side}
        theme="dark"
        mode="vertical">
        <Menu.Item key="1"><Icon type="inbox"/>我要寄件</Menu.Item>
        <Menu.Item key="2"><Icon type="inbox"/>物流信息</Menu.Item>
        <Menu.Item key="3"><Icon type="inbox"/>物流费率</Menu.Item>
        <Menu.Item key="4"><Icon type="environment-o"/>我的地址</Menu.Item>
        <Menu.Item key="5"><Icon type="team"/>我的客户</Menu.Item>
      <Menu.Item key="6"><Icon type="inbox"/>我的账单</Menu.Item>
      </Menu>
        <div className={styles.main}>
          {children}
        </div>
      </div>
    </div>
  );
};

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
};

export default MainLayout;
