import React, { Component, PropTypes } from 'react';
import Home from './home/home';
import MainLayout from '../layouts/MainLayout/MainLayout';

const App = ({ location }) => {
  return (
    <MainLayout>
      {/*<Todos location={location} />*/}
      <Home/>
    </MainLayout>
  );
};

App.propTypes = {
};

export default App;
