import React, {Component, PropTypes} from 'react';
import {Row, Col} from 'antd';
import styles from './home.less';

const Home = () => {
  return (
    <Row>
      <Col span={18}>
        <Col span={12} className={styles.normal}>
          <div className="normal-box">
            <div>
              <h3>总消费</h3>
              <span>¥987.60</span>
            </div>
            <div>
              <div>待付款</div>
            </div>
            <div>
              <div>优惠券</div>
            </div>
            <div>
              <div>客 户</div>
            </div>
          </div>
        </Col>
        <Col span={12} className={styles.normal}>
          <h3>通知</h3>
        </Col>
      </Col>
      <Col span={6} className={styles.normal}>
        <h3>优惠券</h3>
      </Col>
    </Row>
  );
};
export default Home;
